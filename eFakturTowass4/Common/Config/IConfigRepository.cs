﻿namespace eFakturTowass4.Common.Config
{
    public interface IConfigRepository
    {
        
        Models.Config GetConfigValuesEFB(string configKey);
    }
}
