﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace eFakturTowass4.Common.Function
{
  public class FtpWorker : IDisposable
  {
    private FtpWebRequest f = (FtpWebRequest) null;
    private readonly List<string> reply = new List<string>();
    private readonly List<string> errs = new List<string>();

    public bool Delete(string username, string password, string url, ref string errMessage)
    {
      try
      {
        Uri requestUri = new Uri(url);
        if (requestUri.Scheme != Uri.UriSchemeFtp)
          return false;
        FtpWebRequest ftpWebRequest = (FtpWebRequest) WebRequest.Create(requestUri);
        ftpWebRequest.Credentials = (ICredentials) new NetworkCredential(username, password);
        ftpWebRequest.KeepAlive = false;
        ftpWebRequest.Proxy = (IWebProxy) new WebProxy();
        ftpWebRequest.Method = "DELE";
        string str = string.Empty;
        FtpWebResponse response = (FtpWebResponse) ftpWebRequest.GetResponse();
        long contentLength = response.ContentLength;
        Stream responseStream = response.GetResponseStream();
        StreamReader streamReader = new StreamReader(responseStream);
        str = streamReader.ReadToEnd();
        streamReader.Close();
        responseStream.Close();
        response.Close();
        return true;
      }
      catch (Exception ex)
      {
        errMessage = ex.Message;
      }
      return false;
    }

    public byte[] ftpDownloadByte(
      string username,
      string password,
      string url,
      ref string errMessage)
    {
      try
      {
        errMessage = "";
        Uri requestUri = new Uri(url);
        if (requestUri.Scheme != Uri.UriSchemeFtp)
          return (byte[]) null;
        FtpWebRequest ftpWebRequest = (FtpWebRequest) WebRequest.Create(requestUri);
        ftpWebRequest.Method = "RETR";
        ftpWebRequest.Credentials = (ICredentials) new NetworkCredential(username, password);
        FtpWebResponse response = (FtpWebResponse) ftpWebRequest.GetResponse();
        Stream responseStream = response.GetResponseStream();
        byte[] numArray = (byte[]) null;
        using (MemoryStream destination = new MemoryStream())
        {
            if (responseStream != null) responseStream.CopyTo((Stream)destination);
            numArray = destination.ToArray();
        }

        if (responseStream != null) responseStream.Close();
        response.Close();
        return numArray;
      }
      catch (Exception ex)
      {
        errMessage = ex.Message;
      }
      return (byte[]) null;
    }

    public bool Upload(
      string username,
      string password,
      byte[] fileBytes,
      string filename,
      string uploadPath,
      ref bool result,
      ref string errMessage)
    {
      result = false;
      string uri1 = uploadPath;
      int length = filename.LastIndexOf(Path.DirectorySeparatorChar);
      string str = length > 0 ? filename.Substring(0, length) : "";
      string filename1 = length > 0 ? filename.Substring(length + 1, filename.Length - length - 1) : filename;
      bool? nullable = this.Has(username, password, uri1, filename1);
      result = !nullable.GetValueOrDefault();
      if (!nullable.HasValue)
      {
        errMessage = "Connecting to ftp failed";
        result = false;
      }
      else
      {
        if (!result)
        {
          this.MakeDirs(username, password, uri1);
          string uri2 = uri1 + "/" + filename1;
          this.Connect(username, password, uri2);
          this.f.Method = "DELE";
          this.f.GetResponse().Close();
        }
        this.MakeDirs(username, password, uri1);
        string uri3 = uri1 + "/" + filename1;
        this.Connect(username, password, uri3);
        this.f.Method = "STOR";
        Stream requestStream = this.f.GetRequestStream();
        requestStream.Write(fileBytes, 0, fileBytes.Length);
        requestStream.Close();
        result = this.Fetch();
      }
      return result;
    }

    public bool MakeDirs(string username, string password, string uri)
    {
      bool flag1 = false;
      int length1 = uri.Length;
      if (this.Has(username, password, uri).GetValueOrDefault())
        return true;
      int length2 = uri.IndexOf("/", 7, StringComparison.Ordinal);
      int length3 = uri.IndexOf("/", length2 + 1, StringComparison.Ordinal);
      var lastIndexOf = uri.LastIndexOf("/", length1 - 1, StringComparison.Ordinal);
      bool flag2 = false;
      bool? nullable;
      for (; length3 < length1 && length3 > 0; length3 = uri.IndexOf("/", length3 + 1, StringComparison.Ordinal))
      {
        string uri1 = uri.Substring(0, length2);
        string filename = uri.Substring(length2 + 1, length3 - length2 - 1);
        int num;
        if (!flag2)
        {
          nullable = this.Has(username, password, uri1, filename);
          num = (nullable.HasValue ? new bool?(!nullable.GetValueOrDefault()) : new bool?()).GetValueOrDefault() ? 1 : 0;
        }
        else
          num = 1;
        if (num != 0)
        {
          flag1 = this.MD(username, password, uri.Substring(0, length3));
          flag2 = true;
        }
        length2 = length3;
      }
      int num1;
      if (!flag2)
      {
        nullable = this.Has(username, password, uri);
        num1 = (nullable.HasValue ? new bool?(!nullable.GetValueOrDefault()) : new bool?()).GetValueOrDefault() ? 1 : 0;
      }
      else
        num1 = 1;
      if (num1 != 0)
        this.MD(username, password, uri);
      return flag1;
    }

    private bool MD(string username, string password, string uri)
    {
      bool flag = false;
      try
      {
        this.Connect(username, password, uri);
        this.f.Method = "MKD";
        flag = this.Fetch();
      }
      catch (Exception ex)
      {
        this.err(ex);
      }
      return flag;
    }

    private bool? Has(string username, string password, string uri, string filename = "")
    {
      bool? nullable = new bool?();
      string uri1 = uri;
      if (!uri.Substring(uri.Length - 1, 1).Equals("/"))
        uri1 = uri + "/";
      try
      {
        this.Connect(username, password, uri1);
        this.f.Method = "NLST";
        if (this.Fetch())
        {
          if (string.IsNullOrEmpty(filename) && this.reply.Count > 0)
          {
            nullable = new bool?(true);
          }
          else
          {
            nullable = new bool?(false);
            foreach (string strA in this.reply)
            {
              if (String.Compare(strA, filename, StringComparison.OrdinalIgnoreCase) == 0)
              {
                nullable = new bool?(true);
                break;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        this.err(ex);
      }
      string str = uri1 + filename;
      return nullable;
    }

    private bool Fetch()
    {
      bool flag = false;
      this.reply.Clear();
      try
      {
        using (WebResponse response = this.f.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()))
          {
            string str;
            while ((str = streamReader.ReadLine()) != null)
              this.reply.Add(str);
          }
        }
        flag = true;
      }
      catch (WebException ex)
      {
        if (!((IEnumerable<WebExceptionStatus>) new WebExceptionStatus[5]
        {
          WebExceptionStatus.ConnectFailure,
          WebExceptionStatus.ConnectionClosed,
          WebExceptionStatus.Timeout,
          WebExceptionStatus.TrustFailure,
          WebExceptionStatus.ProxyNameResolutionFailure
        }).Contains<WebExceptionStatus>(ex.Status))
          flag = true;
        this.err((Exception) ex);
      }
      catch (Exception ex)
      {
        this.err(ex);
      }
      return flag;
    }

    private void err(Exception ex) => this.errs.Add(ex.Message);

    private void err(string s, params object[] x)
    {
      if (x != null)
        this.errs.Add(string.Format(s, x));
      else
        this.errs.Add(s);
    }

    private void Connect(string username, string password, string uri, bool keepAlive = true)
    {
      this.f = (FtpWebRequest) WebRequest.Create(uri);
      this.f.Credentials = (ICredentials) new NetworkCredential(username, password);
      this.f.KeepAlive = keepAlive;
      this.f.UsePassive = true;
      this.f.UseBinary = true;
    }

    public void Dispose()
    {
    }
  }
}
