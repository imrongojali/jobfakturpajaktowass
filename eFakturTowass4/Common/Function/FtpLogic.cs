﻿using System;
using System.IO;

namespace eFakturTowass4.Common.Function
{
    public class FtpLogic
    {
        public string Directory;

        public bool ftpUploadBytes(
          string username,
          string password,
          string server,
          string path,
          string filename,
          byte[] fileBytes,
          ref bool result,
          ref string errMessage)
        {
            result = false;
            this.Directory = "ftp://" + server + path;
            int length = filename.LastIndexOf(Path.DirectorySeparatorChar);
            string str1 = length > 0 ? filename.Substring(0, length) : "";
            string str2 = length > 0 ? filename.Substring(length + 1, filename.Length - length - 1) : filename;
            FtpWorker ftpWorker = new FtpWorker();
            result = ftpWorker.Upload(username, password, fileBytes, filename, this.Directory, ref result, ref errMessage);
            return result;
        }

        public byte[] ftpDownloadFile(
          string username,
          string password,
          string server,
          string path,
          string filename,
          ref string errMessage)
        {
            string str = "ftp://" + server;
            if (!string.IsNullOrWhiteSpace(path))
                str = str + "/" + path;
            string url = str + "/" + filename;
            return new FtpWorker().ftpDownloadByte(username, password, url, ref errMessage);
        }

        public byte[] ftpDownloadFileWCAR(
          string username,
          string password,
          string server,
          string path,
          string filename,
          ref string errMessage)
        {
            string str = "ftp://" + server;
            if (!string.IsNullOrWhiteSpace(path))
                str += path;
            string url = str + filename;
            return new FtpWorker().ftpDownloadByte(username, password, url, ref errMessage);
        }

        public (bool Res, string ErrMsg) ftpDeleteFile(
          string username,
          string password,
          string server,
          string path,
          string filename,
          ref string errMessage)
        {
            bool flag = false;
            string str = string.Empty;
            string url = "ftp://" + server + "/" + path + "/" + filename;
            FtpWorker ftpWorker = new FtpWorker();
            try
            {
                flag = ftpWorker.Delete(username, password, url, ref errMessage);
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return (flag, str);
        }
    }
}
