﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eFakturTowass4.Common.Function
{
    
    public class CustomException : SystemException
    {
        private readonly string msg;

        public CustomException(string str) : base(str)
        {
            msg = str + "Error System";
        }
    }
}
