﻿using System;
using System.Collections.Generic;
using System.Linq;
using eFakturTowass4.Models.FakturPajak;

namespace eFakturTowass4.Common.VatIn
{
    public interface IVatInRepositories
    {
        List<GetFaktur> GetFileFaktur();
        List<GetXMLFile> GetXMLFile();
        bool GetActiveGenerateXML();
        void UpdateDjpXmlFile(Int64 id, string errorMessage);
        void CreateXMLFile(Int64 idVatIn, string invoiceNo, string fileNameXML, string urlDJPQR, string fileNameDJP);
        void SubmitFaktur(VatInManualInputView param, string urldjp);
        void SubmitFakturTms(VatInManualInputView param, string urldjp);
        void UpdateRfp(Int64 id);
        void ValidasiFakturPajak(Int64 id, string invoceNo, string msg);
        ResultDetailPpn ValidasiFakturPajakPpn(Int64 id, string invoceNo, string fp, string nofp, decimal? dpp, decimal? ppn);

        Result ValidateVATInInvoiceNumber(string nomorFakturGabungan,
            string nomorFaktur,
            string fGPengganti,
            string fakturType,
            string nPWPPenjual,
            string invoiceDate);
        
    }

   
}
