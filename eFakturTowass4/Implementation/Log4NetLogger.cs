﻿
using eFakturTowass4.Common;
using System;
using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eFakturTowass4.Implementation
{
  

    public class Log4NetLogger : ILogger4
    {
        #region Fields

        private readonly ILog log4NetReal = LogManager.GetLogger(typeof(Log4NetLogger));
        private static Log4NetLogger thisLogger;
        #endregion

        #region Properties

        public static Log4NetLogger Current
        {
            get
            {
                if (thisLogger == null)
                {
                    thisLogger = new Log4NetLogger();
                }

                return thisLogger;
            }
        }

        #endregion

        #region Debug
        public void Debug(object message)
        {
            log4NetReal.Debug(message);
        }

        public void Debug(object message, Exception exceptionData)
        {
            log4NetReal.Debug(message, exceptionData);
        }
        #endregion

        #region Error
        public void Error(object message)
        {
            log4NetReal.Error(message);
        }

        public void Error(object message, Exception exceptionData)
        {
            log4NetReal.Error(message, exceptionData);
        }
        #endregion

        #region Fatal
        public void Fatal(object message)
        {
            log4NetReal.Fatal(message);
        }

        public void Fatal(object message, Exception exceptionData)
        {
            log4NetReal.Fatal(message, exceptionData);
        }
        #endregion

        #region Info
        public void Info(object message)
        {
            log4NetReal.Info(message);
        }

        public void Info(object message, Exception exceptionData)
        {
            log4NetReal.Info(message, exceptionData);
        }
        #endregion

        #region Warn
        public void Warn(object message)
        {
            log4NetReal.Warn(message);
        }

        public void Warn(object message, Exception exceptionData)
        {
            log4NetReal.Warn(message, exceptionData);
        }
        #endregion
    }
}
