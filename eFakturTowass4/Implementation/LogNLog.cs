﻿
using eFakturTowass4.Common;
using NLog;

namespace eFakturTowass4.Implementation
{
    public class LogNLog : ILog
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
      //  private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

      public void Information(string message)
        {
            Logger.Info(message);
        }

        public void Warning(string message)
        {
            Logger.Warn(message);
        }

        public void Debug(string message)
        {
            Logger.Debug(message);
        }

        public void Error(string message)
        {
            Logger.Error(message);
        }
    }
}
