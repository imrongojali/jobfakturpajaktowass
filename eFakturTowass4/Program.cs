﻿using eFakturTowass4.Jobs;
using eFakturTowass4.Models;
using eFakturTowass4.Schedular;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using eFakturTowass4.Repositories;
using NLog;


//[assembly: XmlConfigurator(ConfigFile = @"..\..\app.config", Watch = true)]
namespace eFakturTowass4
{

   
    public abstract class Program
    {


       
        public static void Main(string[] args)
        {


            var logger = LogManager.GetCurrentClassLogger();
            try
            {
                logger.Debug("Application Starting Up");
               // CreateHostBuilder(args).Build().Run();
                IHost host = CreateHostBuilder(args).Build();

                 host.Run();


            }
            catch (Exception exception)
            {
                //NLog: catch setup errors
                logger.Error(exception, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                LogManager.Shutdown();
            }
        }


        private static IHostBuilder CreateHostBuilder(string[] args) =>
         Host.CreateDefaultBuilder(args)
              .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    //https://www.youtube.com/watch?v=wldIqJLqJ50
                    services.AddInfrastructure();
                    
                    #region Adding JobType
                    services.AddSingleton<NotificationJob>();
                    services.AddSingleton<GenerateXMLJob>();
                    #endregion

                    #region Adding Jobs 
                    List<JobMetadata> jobMetadatas = new List<JobMetadata>();
                    string efakturJob = System.Configuration.ConfigurationManager.AppSettings["EfakturJob"];
                    string efakturGenerateXMLJob = System.Configuration.ConfigurationManager.AppSettings["EfakturGenerateXMLJob"];
                    string activeefakturJob = System.Configuration.ConfigurationManager.AppSettings["ActiveEfakturJob"];
                    string activeefakturGenerateXMLJob = System.Configuration.ConfigurationManager.AppSettings["ActiveEfakturGenerateXMLJob"];
                    if (activeefakturJob == "1")
                    {
                        jobMetadatas.Add(new JobMetadata(Guid.NewGuid(), typeof(NotificationJob), "Efaktur Job", efakturJob));
                    }
                    if (activeefakturGenerateXMLJob == "1")
                    {
                        jobMetadatas.Add(new JobMetadata(Guid.NewGuid(), typeof(GenerateXMLJob), "Efaktur Job Generate XML", efakturGenerateXMLJob));
                    }

                    services.AddSingleton(jobMetadatas);
                    #endregion

                    services.AddHostedService<EFakturSchedular>();
                });
    }
}
