﻿using eFakturTowass4.Common;
using eFakturTowass4.Common.Config;
using eFakturTowass4.Common.Function;
using eFakturTowass4.Common.VatIn;
using eFakturTowass4.Models.FakturPajak;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;

namespace eFakturTowass4.Jobs
{

    class GenerateXMLJob : IJob
    {
        private readonly IVatInRepositories vatInRepositories;
        private readonly ILog nloglogger;
        private readonly IConfigRepository configRepository;
        private static readonly object Locker;
        protected FtpLogic ftp = new FtpLogic();

        public GenerateXMLJob(ILogger<GenerateXMLJob> logger,
            ILog nloglogger,
            IVatInRepositories vatInRepositories,
            IConfigRepository configRepository)
        {
            this.nloglogger = nloglogger;
            this.vatInRepositories = vatInRepositories;
            this.configRepository = configRepository;
        }
        Task IJob.Execute(IJobExecutionContext context)
        {
            try
            {

                object locker = GenerateXMLJob.Locker;
                bool lockTaken = false;
                try
                {
                    Monitor.Enter(locker, ref lockTaken);
                    this.nloglogger.Debug($"XML Job Run at {DateTime.Now}");
                    bool? _IsActiveJob = false;
                     _IsActiveJob= vatInRepositories.GetActiveGenerateXML();
                    bool IsActiveJob = (bool)((_IsActiveJob == null) ? false : _IsActiveJob);
                    if (IsActiveJob)
                    {
                        this.ReadXml();
                    }
                    else
                    {
                        this.nloglogger.Debug($"XML Job Not Active {DateTime.Now}");
                    }
                }
                finally
                {
                    if (lockTaken)
                        Monitor.Exit(locker);
                }
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.nloglogger.Error(ex.Message);
                return Task.CompletedTask;
            }
        }
        static GenerateXMLJob()
        {
            Locker = new object();
        }
        private void ReadXml()
        {
            foreach (GetXMLFile getXMLFile in this.vatInRepositories.GetXMLFile())
            {
                //GetFaktur getFaktur = vatInRepositories.GetFileFaktur();

                if (getXMLFile != null)
                {
                    try
                    {
                        string XMLFilePath = ConfigurationManager.AppSettings["XMLFilePath"];
                        string XMLFileFullPath = $@"{XMLFilePath}{getXMLFile.FileNameXML}";
                        string xml = File.ReadAllText($@"{XMLFileFullPath}");
                        var LengthNpwpNew = configRepository.GetConfigValuesEFB("LengthNpwpNew");
                        var FormatNpwpNew = configRepository.GetConfigValuesEFB("FormatNpwpNew");//"XXX.XXX.XXX.X-XXX.XXX"
                        var CutOffNpwp = configRepository.GetConfigValuesEFB("CutOffNpwp");
                        //DateTime _cutoff = DateTime.ParseExact("24/01/2013", "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime _cutoff = DateTime.ParseExact(CutOffNpwp.ConfigValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        VatInManualInputView model = new VatInManualInputView();
                        ResValidateFakturPm validateFakturPm;


                        validateFakturPm = xml.ParseXML<ResValidateFakturPm>();


                        if (string.IsNullOrEmpty(validateFakturPm.nomorFaktur))
                            throw new Exception("Invalid URL, Invoice not found");





                        DateTime dateTime1 = Convert
                        .ToDateTime(
                            validateFakturPm.tanggalFakturDateFormat.FormatSqlDate("yyyy-MM-dd"))
                        .AddDays(1.0).AddMonths(3);
                        DateTime dateTime2 = dateTime1.AddDays(-1.0);
                        dateTime1 = new DateTime(dateTime2.Year, dateTime2.Month, 1);
                        dateTime1 = dateTime1.AddMonths(1);
                        DateTime dateTime3 = dateTime1.AddDays(-1.0);

                        model.InvoiceNumber = validateFakturPm.nomorFaktur;
                        model.InvoiceNumberFull =
                            validateFakturPm.nomorFaktur.FormatNomorFakturGabungan(
                                validateFakturPm.kdJenisTransaksi, validateFakturPm.fgPengganti);
                        model.kdJenisTransaksi = validateFakturPm.kdJenisTransaksi;
                        model.fgPengganti = validateFakturPm.fgPengganti;
                        model.InvoiceDate = validateFakturPm.tanggalFakturDateFormat.FormatSqlDate("yyyy-MM-dd");
                        DateTime _tglfaktur = DateTime.ParseExact(validateFakturPm.tanggalFakturDateFormat.FormatSqlDate("yyyy-MM-dd"), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        model.ExpireDate = dateTime3.ToShortDateString();

                       
                       // model.supplierNpwp = validateFakturPm.npwpPenjual.FormatNpwp();
                        model.SupplierName = validateFakturPm.namaPenjual;
                        model.SupplierAddress = validateFakturPm.alamatPenjual;
                        if (_cutoff < _tglfaktur)
                        {
                            model.supplierNpwp= validateFakturPm.npwpPenjual.FormatNpwpCutOff(_tglfaktur, _cutoff, Convert.ToInt32(LengthNpwpNew.ConfigValue), FormatNpwpNew.ConfigValue);
                            model.npwpLawanTransaksi = validateFakturPm.npwpLawanTransaksi.FormatNpwpCutOff(_tglfaktur, _cutoff, Convert.ToInt32(LengthNpwpNew.ConfigValue), FormatNpwpNew.ConfigValue);
                        }
                        else
                        {
                            model.supplierNpwp = validateFakturPm.npwpPenjual.FormatNpwp();
                            model.npwpLawanTransaksi = validateFakturPm.npwpLawanTransaksi.FormatNpwp();
                        }
                        model.NamaLawanTransaksi = validateFakturPm.namaLawanTransaksi;
                        model.AlamatLawanTransaksi = validateFakturPm.alamatLawanTransaksi;
                        model.statusApprovalXml = validateFakturPm.statusApproval;
                        model.statusFakturXml = validateFakturPm.statusFaktur;
                        model.vatBaseAmount = new Decimal?(validateFakturPm.jumlahDpp);
                        model.vatAmount = new Decimal?(validateFakturPm.jumlahPpn);
                        model.jumlahPPnBm = new Decimal?(validateFakturPm.jumlahPpnBm);
                        model.FakturType = "eFaktur";
                        model.vatInDetails = new List<VatInDetailManualInputView>();
                        ResValidateFakturPmDetailTransaksi[] detailTransaksi =
                            validateFakturPm.detailTransaksi;
                        for (int index2 = 0; index2 < detailTransaksi.Length; ++index2)
                        {
                            ResValidateFakturPmDetailTransaksi pmDetailTransaksi =
                                detailTransaksi[index2];
                            VatInDetailManualInputView detailManualInputView =
                                new VatInDetailManualInputView();
                            detailManualInputView.NamaBarang = pmDetailTransaksi.nama;
                            detailManualInputView.HargaSatuan =
                                new Decimal?(pmDetailTransaksi.hargaSatuan);
                            detailManualInputView.JumlahBarang =
                                new Decimal?(pmDetailTransaksi.jumlahBarang);
                            detailManualInputView.HargaTotal =
                                new Decimal?(pmDetailTransaksi.hargaTotal);
                            detailManualInputView.Diskon = new Decimal?(pmDetailTransaksi.diskon);
                            detailManualInputView.dpp = new Decimal?(pmDetailTransaksi.dpp);
                            detailManualInputView.ppn = new Decimal?(pmDetailTransaksi.ppn);
                            detailManualInputView.ppnbm = new Decimal?(pmDetailTransaksi.ppnbm);
                            detailManualInputView.tarifPpnbm =
                                new Decimal?(pmDetailTransaksi.tarifPpnbm);
                            model.vatInDetails.Add(detailManualInputView);

                        }
                        ResultDetailPpn resultDetailPpn =
                                          vatInRepositories.ValidasiFakturPajakPpn(getXMLFile.IdVatIn, getXMLFile.InvoiceNo,
                                              model.InvoiceNumberFull, model.InvoiceNumber, model.vatBaseAmount,
                                              model.vatAmount);
                        if (resultDetailPpn.StatusCode != 0)
                            throw new CustomException(resultDetailPpn.MessageError);
                        model.IdRfp = getXMLFile.IdVatIn;
                        model.InvoiceNoRfp= getXMLFile.InvoiceNo;
                        model.FileName = getXMLFile.FileNameDJP;
                        GenerateXml(model, getXMLFile.UrlDJPQR, getXMLFile.FileNameDJP, getXMLFile.IdVatIn, _cutoff, _tglfaktur);
                        vatInRepositories.UpdateDjpXmlFile(getXMLFile.Id, "Success");

                    }
                    catch (Exception ex)
                    {
                        vatInRepositories.ValidasiFakturPajak(getXMLFile.IdVatIn, getXMLFile.InvoiceNo, ex.Message);
                        nloglogger.Error($"ID {getXMLFile.IdVatIn}, Invoice No : {getXMLFile.InvoiceNo} {ex.Message}");
                        vatInRepositories.UpdateDjpXmlFile(getXMLFile.Id, ex.Message);
                    }

                    nloglogger.Information(
                        "=====================================================================================");
                }
            }
        }

        private void GenerateXml(VatInManualInputView model, string valueXml, string filepdf, Int64 id, DateTime _cutoff, DateTime _tglfaktur)
        {



            try
            {
                //var LengthNpwpNew=configRepository.GetConfigValuesEFB("LengthNpwpNew");
               
                var companyNpwp = (_cutoff < _tglfaktur) ? configRepository.GetConfigValuesEFB("CompanyNPWPNew") : configRepository.GetConfigValuesEFB("CompanyNPWP");
                var configbatam = (_cutoff < _tglfaktur) ? configRepository.GetConfigValuesEFB("CompanyNPWPBatamNew") : configRepository.GetConfigValuesEFB("CompanyNPWPBatam");
                var nilaiSelisihHari = configRepository.GetConfigValuesEFB("VATInDaysExpired");
                if (model.FakturType == "eFaktur" && !string.IsNullOrEmpty(valueXml))
                {
                    #region Validasi VAT In khusus scan
                    //validasi npwp

                    if (companyNpwp == null)
                    {

                        throw new CustomException("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new CustomException("Please define the CompanyNPWPBatam Config");
                    }

                  

                    if (!model.npwpLawanTransaksi.Equals(companyNpwp.ConfigValue) && !model.npwpLawanTransaksi.Equals(configbatam.ConfigValue))
                    {
                        throw new CustomException((_cutoff < _tglfaktur) ? 
                            $"Npwp Company {model.npwpLawanTransaksi} Company yang diupload harus format {companyNpwp.ConfigValue.Length} digit, tanggal faktur {_tglfaktur.FormatSqlDate("dd-MM-yyyy")} sudah melewati cut off {_cutoff.FormatSqlDate("dd-MM-yyyy")}"
                            : $"Buyer NPWP {model.npwpLawanTransaksi} does not match with Company NPWP");
                    }

                    //validasi status faktur
                    if (model.statusFakturXml == "Faktur Dibatalkan")
                    {
                        throw new CustomException("Cannot save Data with Status Faktur Dibatalkan");
                    }

                    //validasi expire date
                    string invoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(invoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int selisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;


                    if (selisihHari < Convert.ToInt32(nilaiSelisihHari.ConfigValue))
                    {
                        if (selisihHari <= 0)
                        {
                            selisihHari = 0;
                        }

                        throw new CustomException("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + selisihHari + " Days Before Expired.");
                    }

                    //validasi Faktur pengganti
                    //Result validateResult = vatInRepositories.ValidateVATInInvoiceNumber(model.InvoiceNumberFull, model.InvoiceNumber, model.fgPengganti, model.FakturType, model.supplierNpwp, model.InvoiceDate);

                    //if (!validateResult.ResultCode)
                    //{
                    //    throw new CustomException(validateResult.ResultDesc);
                    //}

                    #endregion
                }
                vatInRepositories.SubmitFaktur(model, valueXml);
                vatInRepositories.SubmitFakturTms(model, valueXml);

               // vatInRepositories.UpdateDjpXmlFile(id, "Success");
                //string appSettingIsFtp = ConfigurationManager.AppSettings["IsFTP"];
                //if (appSettingIsFtp == "1")
                //{
                //    try
                //    { 
                //        string ftpUser = ConfigurationManager.AppSettings["FtpUser"];
                //    string ftpPassword = ConfigurationManager.AppSettings["FtpPassword"];
                //    string ftpServer = ConfigurationManager.AppSettings["FtpServer"];
                //    string ftpFolder = ConfigurationManager.AppSettings["FtpFolder"];
                //    bool result = true;
                //    string errorMessage = null;
                //    //string fileName = Path.GetFileName(filepdf);
                //    StreamReader sourceStream = new StreamReader(filepdf);
                //    byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                //    sourceStream.Close();

                //    //string fileName = Path.GetFileNameWithoutExtension(@"C:\Program Files\hello.txt");
                //    //This will return "hello" for fileName.
                //    bool resfile = ftp.ftpUploadBytes(ftpUser, ftpPassword, ftpServer, ftpFolder, model.FileName, fileContents, ref result,
                //        ref errorMessage);
                //    if (!resfile)
                //    {

                //        throw new CustomException(errorMessage);

                //    }
                //    }
                //    catch (Exception e)
                //    {
                //        throw new CustomException(e.Message.ToString());
                //    }
                //}


                // https://www.c-sharpcorner.com/UploadFile/97fc7a/upload-the-file-on-file-transfer-protocol-ftp/


            }
            catch (Exception e)
            {

                throw new CustomException(e.Message);
            }
        }
    }
}
