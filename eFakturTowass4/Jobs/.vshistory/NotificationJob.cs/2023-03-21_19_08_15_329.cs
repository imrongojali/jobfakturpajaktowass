﻿using Bytescout.BarCodeReader;
using Dapper;
using eFakturTowass4.Common;
using eFakturTowass4.Common.Config;
using eFakturTowass4.Common.Function;
using eFakturTowass4.Models.FakturPajak;
using eFakturTowass4.Repositories.VatIn;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using Tesseract.Interop;
using System.Threading;
using Colorful;
using System.Collections;
using System.Configuration;
using Polly;
using System.Web.UI.WebControls;
using Polly.Retry;

namespace eFakturTowass4.Jobs
{
    class NotificationJob : IJob
    {
        private readonly ILogger<NotificationJob> logger;
 
        private readonly IVatInRepositories vatInRepositories;
        private readonly ILog nloglogger;
        private readonly IConfigRepository configRepository;
        private static readonly object Locker;

        public NotificationJob(ILogger<NotificationJob> logger, 
            ILog nloglogger, 
            IVatInRepositories vatInRepositories,
            IConfigRepository configRepository)
        {
            this.logger = logger;
           
            this.nloglogger = nloglogger;
            this.vatInRepositories = vatInRepositories;
            this.configRepository= configRepository;
        }
        Task IJob.Execute(IJobExecutionContext context)
        {

            try
            {
                this.nloglogger.Information(string.Format("Efaktur Job Run at {0}", (object)DateTime.Now));
                object locker = NotificationJob.Locker;
                bool lockTaken = false;
                try
                {
                    Monitor.Enter(locker, ref lockTaken);
                    this.Getdataqr();
                }
                finally
                {
                    if (lockTaken)
                        Monitor.Exit(locker);
                }
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.nloglogger.Error(ex.Message);
                return Task.CompletedTask;
            }


        }
        private void Getdataqr()
        {
            GetFaktur getFaktur = this.vatInRepositories.GetFileFaktur().FirstOrDefault<GetFaktur>();
            if (getFaktur != null)
            {
                try
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    stopwatch.Start();
                    this.nloglogger.Information(string.Format("Job Run! ID {0}, Invoice No : {1}", (object)getFaktur.Id, (object)getFaktur.InvoiceNo));
                    string appSetting = ConfigurationManager.AppSettings["QRPDFFilesDJP"];
                    string fullPath = Path.GetFullPath(string.Concat(appSetting, getFaktur.FileName));
                    if (!File.Exists(fullPath))
                        fullPath = Path.GetFullPath(string.Concat(appSetting, getFaktur.InvoiceNo, "\\", getFaktur.FileName));
                    this.nloglogger.Information(string.Format("ID {0}, Invoice No : {1} File Path : {2}", (object)getFaktur.Id, (object)getFaktur.InvoiceNo, (object)fullPath));
                    if (!File.Exists(fullPath))
                    {
                        this.nloglogger.Information(string.Concat("File Path : ", fullPath));
                        this.vatInRepositories.updateRfp(getFaktur.Id);
                        throw new Exception(string.Format("ID {0}, Invoice No : {1} Nama File : {2} Tidak di temukan", (object)getFaktur.Id, (object)getFaktur.InvoiceNo, (object)getFaktur.FileName));
                    }
                    this.nloglogger.Information(string.Concat("File Name : ", fullPath));
                    Reader reader = new Reader("1_DEVELOPER_LICENSE_FOR_DESKTOP_DEVELOPER_LICENSE_2D_BARCODES_ONLY_DESKTOP_APPS_ONLY_MAHENDRA.SONDAY@EMERIOCORP.COM_SUPPORT_AND_UPDATES_UNTIL_APRIL_25_2020", "E702-4936-D399-2BC9-8239-D07D-27B");
                    reader.ImagePreprocessingFilters.AddScale(0.75);
                    reader.BarcodeTypesToFind.QRCode = true;
                    reader.FastMode = true;
                    int pdfPageCount = reader.GetPdfPageCount(fullPath);
                    int num = pdfPageCount > 0 ? pdfPageCount - 1 : 0;
                    int pageIndex1 = 0;
                    for (int pageIndex2 = num; pageIndex2 >= 0; --pageIndex2)
                    {
                        FoundBarcode[] source = reader.ReadFrom(fullPath, pageIndex2, pdfPageCount);
                        if (((IEnumerable<FoundBarcode>)source).Any<FoundBarcode>())
                        {
                            pageIndex1 = source[0].Page;
                            break;
                        }
                    }
                    FoundBarcode[] source1 = reader.ReadFrom(fullPath, pageIndex1, pdfPageCount);
                    if (((IEnumerable<FoundBarcode>)source1).Any<FoundBarcode>())
                    {
                        try
                        {
                            FoundBarcode[] foundBarcodeArray = source1;
                            for (int index1 = 0; index1 < foundBarcodeArray.Length; ++index1)
                            {
                                string str = foundBarcodeArray[index1].Value.ToString();
                                this.nloglogger.Information(string.Concat("DJP Link : ", str));
                                HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
                                VATInManualInputView model = new VATInManualInputView();
                                resValidateFakturPm validateFakturPm1 = new resValidateFakturPm();
                                HttpClient httpClient = new HttpClient();
                                try
                                {
                                    httpClient.DefaultRequestHeaders.Accept.Clear();
                                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                                    Uri requestUri = new Uri(str, UriKind.Absolute);
                                    MemoryStream input = new MemoryStream(httpClient.GetAsync(requestUri).Result.Content.ReadAsByteArrayAsync().Result);
                                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(resValidateFakturPm));
                                    XmlReader xmlReader = XmlReader.Create((Stream)input);
                                   
                                       resValidateFakturPm validateFakturPm2=new resValidateFakturPm();
                                    try
                                    {
                                        validateFakturPm2 = (resValidateFakturPm)xmlSerializer.Deserialize(xmlReader);
                                        
                                        if (string.IsNullOrEmpty(validateFakturPm2.nomorFaktur))
                                           throw new Exception("Invalid URL, Invoice not found");
                                        
                                    }
                                    finally
                                    {
                                        if (xmlReader != null)
                                            xmlReader.Dispose();
                                    }
                                    DateTime dateTime1 = Convert.ToDateTime(validateFakturPm2.tanggalFakturDateFormat.FormatSQLDate("yyyy-MM-dd")).AddDays(1.0);
                                    dateTime1 = dateTime1.AddMonths(3);
                                    DateTime dateTime2 = dateTime1.AddDays(-1.0);
                                    dateTime1 = new DateTime(dateTime2.Year, dateTime2.Month, 1);
                                    dateTime1 = dateTime1.AddMonths(1);
                                    DateTime dateTime3 = dateTime1.AddDays(-1.0);
                                    model.IdRfp = getFaktur.Id;
                                    model.InvoiceNoRfp = getFaktur.InvoiceNo;
                                    model.FileName = getFaktur.FileName;
                                    model.InvoiceNumber = validateFakturPm2.nomorFaktur;
                                    model.InvoiceNumberFull = validateFakturPm2.nomorFaktur.FormatNomorFakturGabungan(validateFakturPm2.kdJenisTransaksi, validateFakturPm2.fgPengganti);
                                    model.KDJenisTransaksi = validateFakturPm2.kdJenisTransaksi;
                                    model.FGPengganti = validateFakturPm2.fgPengganti;
                                    model.InvoiceDate = validateFakturPm2.tanggalFakturDateFormat.FormatSQLDate("yyyy-MM-dd");
                                    model.ExpireDate = dateTime3.ToShortDateString();
                                    model.SupplierNPWP = validateFakturPm2.npwpPenjual.FormatNPWP();
                                    model.SupplierName = validateFakturPm2.namaPenjual;
                                    model.SupplierAddress = validateFakturPm2.alamatPenjual;
                                    model.NPWPLawanTransaksi = validateFakturPm2.npwpLawanTransaksi.FormatNPWP();
                                    model.NamaLawanTransaksi = validateFakturPm2.namaLawanTransaksi;
                                    model.AlamatLawanTransaksi = validateFakturPm2.alamatLawanTransaksi;
                                    model.StatusApprovalXML = validateFakturPm2.statusApproval;
                                    model.StatusFakturXML = validateFakturPm2.statusFaktur;
                                    model.VATBaseAmount = new Decimal?(validateFakturPm2.jumlahDpp);
                                    model.VATAmount = new Decimal?(validateFakturPm2.jumlahPpn);
                                    model.JumlahPPnBM = new Decimal?(validateFakturPm2.jumlahPpnBm);
                                    model.FakturType = "eFaktur";
                                    model.VATInDetails = new List<VATInDetailManualInputView>();
                                    resValidateFakturPmDetailTransaksi[] detailTransaksi = validateFakturPm2.detailTransaksi;
                                    for (int index2 = 0; index2 < detailTransaksi.Length; ++index2)
                                    {
                                        resValidateFakturPmDetailTransaksi pmDetailTransaksi = detailTransaksi[index2];
                                        VATInDetailManualInputView detailManualInputView = new VATInDetailManualInputView();
                                        detailManualInputView.NamaBarang = pmDetailTransaksi.nama;
                                        detailManualInputView.HargaSatuan = new Decimal?(pmDetailTransaksi.hargaSatuan);
                                        detailManualInputView.JumlahBarang = new Decimal?(pmDetailTransaksi.jumlahBarang);
                                        detailManualInputView.HargaTotal = new Decimal?(pmDetailTransaksi.hargaTotal);
                                        detailManualInputView.Diskon = new Decimal?(pmDetailTransaksi.diskon);
                                        detailManualInputView.DPP = new Decimal?(pmDetailTransaksi.dpp);
                                        detailManualInputView.PPN = new Decimal?(pmDetailTransaksi.ppn);
                                        detailManualInputView.PPNBM = new Decimal?(pmDetailTransaksi.ppnbm);
                                        detailManualInputView.TarifPPNBM = new Decimal?(pmDetailTransaksi.tarifPpnbm);
                                        model.VATInDetails.Add(detailManualInputView);
                                    }
                                    ResultDetailPPN resultDetailPpn = this.vatInRepositories.validasiFakturPajakPPN(getFaktur.Id, getFaktur.InvoiceNo, model.InvoiceNumberFull, model.InvoiceNumber, model.VATBaseAmount, model.VATAmount);
                                    if (resultDetailPpn.StatusCode != 0)
                                        throw new Exception(resultDetailPpn.MessageError);
                                    this.GenerateXml(model, str);
                                }
                                finally
                                {
                                    if (httpClient != null)
                                        httpClient.Dispose();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            this.vatInRepositories.validasiFakturPajak(getFaktur.Id, getFaktur.InvoiceNo, ex.Message);

                            //There is an error in XML document(1, 2).
                            //One or more errors occurred.
                            this.nloglogger.Error(string.Format("ID {0}, Invoice No : {1} {2}", (object)getFaktur.Id, (object)getFaktur.InvoiceNo, (object)ex.Message));
                        }
                    }
                    else
                    {
                        this.vatInRepositories.validasiFakturPajak(getFaktur.Id, getFaktur.InvoiceNo, string.Concat("Nama File : ", getFaktur.FileName, " QR COde Tidak Terbaca"));
                        this.nloglogger.Warning(string.Format("ID {0}, Invoice No : {1} Nama File : {2} QR COde Tidak Terbaca", (object)getFaktur.Id, (object)getFaktur.InvoiceNo, (object)getFaktur.FileName));
                    }
                    stopwatch.Stop();
                    this.nloglogger.Information(string.Format("Efaktur Job ID {0}, Invoice No : {1} finished, time elapsed {2}.", (object)getFaktur.Id, (object)getFaktur.InvoiceNo, (object)stopwatch.Elapsed.Seconds));
                }
                catch (Exception ex)
                {
                    this.vatInRepositories.validasiFakturPajak(getFaktur.Id, getFaktur.InvoiceNo, ex.Message);
                    this.nloglogger.Error(string.Format("ID {0}, Invoice No : {1} {2}", (object)getFaktur.Id, (object)getFaktur.InvoiceNo, (object)ex.Message));
                }
            }
            this.nloglogger.Information("=====================================================================================");
        }
       
        static NotificationJob()
        {
            NotificationJob.Locker = new object();
        }


        private void GenerateXml(VATInManualInputView model, string valueXml)
        {



            try
            {
                var companyNpwp = configRepository.GetConfigValues("EFAKTUR", "EFAKTURNPWP", "CompanyNPWP");
                var configbatam = configRepository.GetConfigValues("EFAKTUR", "EFAKTURNPWP", "CompanyNPWPBatam");
                var nilaiSelisihHari = configRepository.GetConfigValues("EFAKTUR", "EFAKTURExpired", "VATInDaysExpired");
                if (model.FakturType == "eFaktur" && !string.IsNullOrEmpty(valueXml))
                {
                    #region Validasi VAT In khusus scan
                    //validasi npwp

                    if (companyNpwp == null)
                    {
                        throw new Exception("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new Exception("Please define the CompanyNPWPBatam Config");
                    }

                    if (!model.NPWPLawanTransaksi.Equals(companyNpwp) && !model.NPWPLawanTransaksi.Equals(configbatam))
                    {
                        throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with Company NPWP");
                    }

                    //validasi status faktur
                    if (model.StatusFakturXML == "Faktur Dibatalkan")
                    {
                        throw new Exception("Cannot save Data with Status Faktur Dibatalkan");
                    }

                    //validasi expire date
                    string invoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(invoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int selisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;


                    if (selisihHari < Convert.ToInt32(nilaiSelisihHari))
                    {
                        if (selisihHari <= 0)
                        {
                            selisihHari = 0;
                        }

                        throw new Exception("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + selisihHari + " Days Before Expired.");
                    }

                    //validasi Faktur pengganti
                    //Result validateResult = executeProcedureQuery<Result>("TAM_EFAKTUR.dbo.usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate }).FirstOrDefault();

                    //if (!validateResult.ResultCode)
                    //{
                    //    throw new Exception(validateResult.ResultDesc);
                    //}

                    #endregion
                }
                vatInRepositories.SubmitFaktur(model, valueXml);



            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        private XmlReader RetryUrlDjp(HttpClient httpClient, string str)
        {

            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
            Uri requestUri = new Uri(str, UriKind.Absolute);
            MemoryStream input = new MemoryStream(httpClient.GetAsync(requestUri).Result.Content.ReadAsByteArrayAsync().Result);
           
            XmlReader xmlReader = XmlReader.Create((Stream)input);
            return xmlReader;
        }
    }
}
