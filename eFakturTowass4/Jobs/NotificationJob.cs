﻿using Bytescout.BarCodeReader;
using eFakturTowass4.Common;
using eFakturTowass4.Common.Config;
using eFakturTowass4.Common.Function;
using eFakturTowass4.Models.FakturPajak;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.Threading;
using System.Configuration;
using eFakturTowass4.Common.VatIn;
using static System.Net.WebRequestMethods;
using System.ComponentModel;
using System.Text;
using File = System.IO.File;
using System.Net;
using System.Security.Policy;
using NLog.LayoutRenderers.Wrappers;

namespace eFakturTowass4.Jobs
{
    class NotificationJob : IJob
    {
        private readonly IVatInRepositories vatInRepositories;
        private readonly ILog nloglogger;
        private readonly IConfigRepository configRepository;
        private static readonly object Locker;
        protected FtpLogic ftp = new FtpLogic();

        public NotificationJob(ILogger<NotificationJob> logger, 
            ILog nloglogger, 
            IVatInRepositories vatInRepositories,
            IConfigRepository configRepository)
        {
            this.nloglogger = nloglogger;
            this.vatInRepositories = vatInRepositories;
            this.configRepository= configRepository;
        }
        Task IJob.Execute(IJobExecutionContext context)
        {

            try
            {
               
                object locker = NotificationJob.Locker;
                bool lockTaken = false;
                try
                {
                    Monitor.Enter(locker, ref lockTaken);
                    this.nloglogger.Debug($"Efaktur Job Run at {DateTime.Now}");
                    this.Getdataqr();
                }
                finally
                {
                    if (lockTaken)
                        Monitor.Exit(locker);
                }
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                this.nloglogger.Error(ex.Message);
                return Task.CompletedTask;
            }


        }
        private void Getdataqr()
        {
            foreach (GetFaktur getFaktur in this.vatInRepositories.GetFileFaktur())
            {
                //GetFaktur getFaktur = vatInRepositories.GetFileFaktur();

                if (getFaktur != null)
                {
                    try
                    {
                        Stopwatch stopwatch = Stopwatch.StartNew();
                        stopwatch.Start();
                        nloglogger.Information($"Job Run! ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo}");
                        string appSetting = ConfigurationManager.AppSettings["QRPDFFilesDJP"];
                        string fullPath = Path.GetFullPath(string.Concat(appSetting, getFaktur.FileName));
                        if (!File.Exists(fullPath))
                            fullPath = Path.GetFullPath(string.Concat(appSetting, getFaktur.InvoiceNo, "\\",
                                getFaktur.FileName));
                        nloglogger.Information(
                            $"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} File Path : {fullPath}");
                        if (!File.Exists(fullPath))
                        {
                            nloglogger.Information(string.Concat("File Path : ", fullPath));
                            //vatInRepositories.UpdateRfp(getFaktur.Id);
                            throw new CustomException(
                                $"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} Nama File : {getFaktur.FileName} Tidak di temukan");
                        }

                        nloglogger.Information(string.Concat("File Name : ", fullPath));
                        Reader reader =
                            new Reader(
                                "1_DEVELOPER_LICENSE_FOR_DESKTOP_DEVELOPER_LICENSE_2D_BARCODES_ONLY_DESKTOP_APPS_ONLY_MAHENDRA.SONDAY@EMERIOCORP.COM_SUPPORT_AND_UPDATES_UNTIL_APRIL_25_2020",
                                "E702-4936-D399-2BC9-8239-D07D-27B");
                        reader.ImagePreprocessingFilters.AddScale(0.75);
                        reader.BarcodeTypesToFind.QRCode = true;
                        reader.FastMode = true;
                        int pdfPageCount = reader.GetPdfPageCount(fullPath);
                        int num = pdfPageCount > 0 ? pdfPageCount - 1 : 0;
                        int pageIndex1 = 0;
                        for (int pageIndex2 = num; pageIndex2 >= 0; --pageIndex2)
                        {
                            FoundBarcode[] source = reader.ReadFrom(fullPath, pageIndex2, pdfPageCount);
                            if (source.Any())
                            {
                                pageIndex1 = source[0].Page;
                                break;
                            }
                        }

                        FoundBarcode[] source1 = reader.ReadFrom(fullPath, pageIndex1, pdfPageCount);
                        if (source1.Any())
                        {
                            try
                            {
                                FoundBarcode[] foundBarcodeArray = source1;
                                for (int index1 = 0; index1 < foundBarcodeArray.Length; ++index1)
                                {
                                    //string str = "http://svc.efaktur.pajak.go.id/validasi/faktur/013025846092000/0002135559622/d99d56f6c64e07f6174eadde375a82d08a6af954ebc44d05b0dd5a0877c3ba83";

                                    string str = foundBarcodeArray[index1].Value;
                                    nloglogger.Information(string.Concat("DJP Link : ", str));



                                    try
                                    {
                                        string IsWebProxy = ConfigurationManager.AppSettings["IsWebProxy"];

                                        ServicePointManager.Expect100Continue = true;
                                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                                    WebClient webClient = new WebClient();

                                        if (IsWebProxy == "1")
                                        {
                                            string SetWebProxyServer = ConfigurationManager.AppSettings["SetWebProxyServer"];
                                          
                                            WebProxy wp = new WebProxy(SetWebProxyServer);
                                            webClient.Proxy = wp;
                                        }
                                        string XMLFilePath = ConfigurationManager.AppSettings["XMLFilePath"];
                                        string XMLFileFullPath = $@"{XMLFilePath}{getFaktur.InvoiceNo}.xml";
                                        webClient.DownloadFile(str, XMLFileFullPath);
                                        this.vatInRepositories.CreateXMLFile(getFaktur.Id, getFaktur.InvoiceNo, $"{getFaktur.InvoiceNo}.xml", str, getFaktur.FileName);
                                       // vatInRepositories.UpdateRfp(getFaktur.Id);
                                        webClient.Dispose();
                                    }
                                    catch (WebException ex)
                                    {
                                       
                                        nloglogger.Error($"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} Downloading {str} failed. {ex.Message}");
                                        throw new Exception($"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} Downloading {str} failed. {ex.Message}");
                                    }
                                    catch (InvalidOperationException)
                                    {
                                      
                                        nloglogger.Error($"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} Saving {str} to {getFaktur.InvoiceNo}.xml failed. File is in use.");
                                        throw new Exception($"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} Saving {str} to {getFaktur.InvoiceNo}.xml failed. File is in use.");
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                vatInRepositories.ValidasiFakturPajak(getFaktur.Id, getFaktur.InvoiceNo, ex.Message);

                                //There is an error in XML document(1, 2).
                                //One or more errors occurred.
                                nloglogger.Error($"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} {ex.Message}");
                            }
                        }
                        else
                        {
                            vatInRepositories.ValidasiFakturPajak(getFaktur.Id, getFaktur.InvoiceNo,
                                string.Concat("Nama File : ", getFaktur.FileName, " QR COde Tidak Terbaca"));
                            nloglogger.Warning(
                                $"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} Nama File : {getFaktur.FileName} QR COde Tidak Terbaca");
                        }

                        stopwatch.Stop();
                        nloglogger.Information(
                            $"Efaktur Job ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} finished, time elapsed {stopwatch.Elapsed.Seconds}.");
                    }
                    catch (Exception ex)
                    {
                        vatInRepositories.ValidasiFakturPajak(getFaktur.Id, getFaktur.InvoiceNo, ex.Message);
                        nloglogger.Error($"ID {getFaktur.Id}, Invoice No : {getFaktur.InvoiceNo} {ex.Message}");
                    }

                    nloglogger.Information(
                        "=====================================================================================");
                }
            }

        }
       
        static  NotificationJob()
        {
            Locker = new object();
        }



    }
}
