﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using eFakturTowass4.Common.Function;

namespace eFakturTowass4.Repositories
{
    public abstract class BaseRepository
    {


      

        protected static IDbConnection OpenConnection()
        {
            var conn = ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString;
            IDbConnection connection = new SqlConnection(conn);
            connection.Open();
            return connection;

        }


      


      
        protected IEnumerable<TRes> ExecuteProcedure<TRes>(string spName, object param, CommandType? commandType = null)
        {
            using (var connection = OpenConnection())
            {
                return connection.Query<TRes>(spName, param, commandType:
                    commandType ?? CommandType.StoredProcedure);
            }
        }
        protected IEnumerable<TRes> ExecuteProcedureWithMsg<TRes>(string spName, object param)
        {
            AddMessageParamForOutput((DynamicParameters)param);
            using (var connection = OpenConnection())
            {
                var ret = connection.Query<TRes>(spName, param, commandType: CommandType.StoredProcedure);
                var dynamicParameters = param as DynamicParameters;
                if (dynamicParameters != null && !dynamicParameters.Get<string>("errMsg").Equals(string.Empty))
                    throw new CustomException((param as DynamicParameters).Get<string>("errMsg"));
                return ret;
            }
        }


        protected static IDbConnection OpenConnectionEFB()
        {
            var conn = ConfigurationManager.ConnectionStrings["SqlServerConnStringEFB"].ConnectionString;
            IDbConnection connection = new SqlConnection(conn);
            connection.Open();
            return connection;

        }






        protected IEnumerable<TRes> ExecuteProcedureEFB<TRes>(string spName, object param, CommandType? commandType = null)
        {
            using (var connection = OpenConnectionEFB())
            {
                return connection.Query<TRes>(spName, param, commandType:
                    commandType ?? CommandType.StoredProcedure);
            }
        }
        protected IEnumerable<TRes> ExecuteProcedureEFBWithMsg<TRes>(string spName, object param)
        {
            AddMessageParamForOutput((DynamicParameters)param);
            using (var connection = OpenConnectionEFB())
            {
                var ret = connection.Query<TRes>(spName, param, commandType: CommandType.StoredProcedure);
                var dynamicParameters = param as DynamicParameters;
                if (dynamicParameters != null && !dynamicParameters.Get<string>("errMsg").Equals(string.Empty))
                    throw new CustomException((param as DynamicParameters).Get<string>("errMsg"));
                return ret;
            }
        }


        private static void AddMessageParamForOutput(DynamicParameters p)
        {
            p.Add("errMsg", dbType: DbType.String, size: 2048, direction: ParameterDirection.Output);
        }
       
    }
}
