﻿using eFakturTowass4.Common;
using eFakturTowass4.Implementation;
using eFakturTowass4.JobFactory;
using eFakturTowass4.Repositories.VatIn;
using Microsoft.Extensions.DependencyInjection;
using Quartz.Impl;
using Quartz.Spi;
using Quartz;
using eFakturTowass4.Common.Config;
using eFakturTowass4.Common.VatIn;
using eFakturTowass4.Repositories.Config;

namespace eFakturTowass4.Repositories
{
    public static class ServiceRegistration
    {
        public static void AddInfrastructure(this IServiceCollection services)
        {
            services.AddSingleton<IJobFactory, JobFakturFactory>();
            //services.AddSingleton<ILogger4, Log4NetLogger>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddTransient<IVatInRepositories, VatInRepositories>();
            services.AddTransient<IConfigRepository, ConfigRepository>();
            
            services.AddSingleton<ILog, LogNLog>();

        }
    }
}
