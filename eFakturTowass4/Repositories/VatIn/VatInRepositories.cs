﻿//using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using eFakturTowass4.Models.FakturPajak;
using eFakturTowass4.Common.Function;
using eFakturTowass4.Common.VatIn;

namespace eFakturTowass4.Repositories.VatIn
{
    public class VatInRepositories : BaseRepository, IVatInRepositories
    {


        //https://www.red-gate.com/simple-talk/development/dotnet-development/a-practical-guide-to-dapper/
        public List<GetXMLFile> GetXMLFile()
        {
            IDbConnection cnn = OpenConnection();
            try
            {
                const string query = "dbo.sp_GetXMLFile";
               // const string query = "SELECT top 1 rf.Id, rf.InvoiceNo,rf.FileName FROM dbo.RFp AS rf WHERE ISNULL(rf.FileName,'')!='' AND ISNULL(rf.FpNo,'')='' AND rf.IsDeleted=0 AND rf.ActiveFlag=1 and status is null  ";
            return cnn.Query<GetXMLFile>(query, null, null, true, 500, CommandType.StoredProcedure).ToList();

            }
            finally
            {
                cnn?.Dispose();
            }

        }

        public bool GetActiveGenerateXML()
        {
            IDbConnection cnn = OpenConnection();
            try
            {
               
                 const string query = "select convert(bit,SystemValue) ActiveGenerateXML from MasterSystemConfig where SystemCategory='GENERATEXML' and SystemSubCategory='ACTIVEGENERATEXML' And SystemCode='RUNJOB' AND Active = 1 AND IsDeleted = 0  ";
                return cnn.Query<bool>(query, null, null, true, 500, CommandType.Text).SingleOrDefault();

            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
            finally
            {
                cnn?.Dispose();
            }

        }

        public void CreateXMLFile(Int64 idVatIn, string invoiceNo, string fileNameXML, string urlDJPQR, string fileNameDJP)
        {
            try
            {
                IDbConnection cnn = OpenConnection();
                try
                {
                    var p = new DynamicParameters();
                    p.Add("@idVatIn", idVatIn);
                    p.Add("@InvoiceNo", invoiceNo);
                    p.Add("@FileNameXML", fileNameXML);
                    p.Add("@UrlDJPQR", urlDJPQR);
                    p.Add("@FileNameDJP", fileNameDJP);


                    cnn.Execute("sp_CreateDjpXmlFile", p, commandTimeout: 500, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }

        public List<GetFaktur> GetFileFaktur()
        {
            try
            {
                IDbConnection cnn = OpenConnection();
                try
                {
                   
                    const string query = "dbo.sp_GetRFpQr";
                // const string query = "SELECT top 1 rf.Id, rf.InvoiceNo,rf.FileName FROM dbo.RFp AS rf WHERE ISNULL(rf.FileName,'')!='' AND ISNULL(rf.FpNo,'')='' AND rf.IsDeleted=0 AND rf.ActiveFlag=1 and status is null  ";
                return cnn.Query<GetFaktur>(query, null, null, true, 500, CommandType.StoredProcedure).ToList();

                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }

        }

        public void UpdateRfp(long id)
        {
            try
            {
                IDbConnection cnn = OpenConnection();
                try
                {
                    const string sql = " update RFp set ActiveFlag=0 , Status='1' where id=@id ";
                    cnn.Query<GetFaktur>(sql, new { id }, commandTimeout: 500, commandType: CommandType.Text);
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }


        public void ValidasiFakturPajak(long id, string invoceNo, string msg)
        {
            try
            {
                IDbConnection cnn = OpenConnection();
                try
                {
                    var p = new DynamicParameters();
                    p.Add("@id", id);
                    p.Add("@InvoceNo", invoceNo);
                    p.Add("@ErrMessage", msg);


                    cnn.Execute("sp_ValidasiInvoceFakturPajak", p, commandTimeout: 500, commandType: CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }


        public ResultDetailPpn ValidasiFakturPajakPpn(
            long id,
            string invoceNo,
            string fp,
            string nofp,
            Decimal? dpp,
            Decimal? ppn)
        {
            try
            {
                IDbConnection cnn = OpenConnection();
                try
                {
                    var p = new DynamicParameters();
                    p.Add("@id", id);
                    p.Add("@InvoceNo", invoceNo);
                    p.Add("@fakturpajak", fp);
                    p.Add("@Nofakturpajak", nofp);
                    p.Add("@dpp", dpp);
                    p.Add("@ppn", ppn);
                    return cnn.Query<ResultDetailPpn>("sp_ValidasiInvoceFakturPajakPPN", p, null, true, 500, CommandType.StoredProcedure).AsEnumerable().FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }


        public void SubmitFaktur(VatInManualInputView param, string urldjp)
        {
            try
            {
                IDbConnection cnn = OpenConnection();
                try
                {
                    Guid vatInId = Guid.NewGuid();
                    Guid syncId = Guid.NewGuid();
                    DateTime expireDate = Convert.ToDateTime(param.ExpireDate.Trim());
                    var p = new DynamicParameters();
                    p.Add("@Id", vatInId);
                    p.Add("@SyncId", syncId);
                    p.Add("@IdRfp", param.IdRfp);
                    p.Add("@InvoiceNo", param.InvoiceNoRfp);
                    p.Add("@FileName", param.FileName);
                    p.Add("@KDJenisTransaksi", param.kdJenisTransaksi);
                    p.Add("@FGPengganti", param.fgPengganti);
                    p.Add("@NomorFaktur", param.InvoiceNumber);
                    p.Add("@NomorFakturGabungan", param.InvoiceNumberFull);
                    p.Add("@TanggalFaktur", param.InvoiceDate);
                    p.Add("@ExpireDate", expireDate);
                    p.Add("@NPWPPenjual", param.supplierNpwp);
                    p.Add("@NamaPenjual", param.SupplierName);
                    p.Add("@AlamatPenjual", param.SupplierAddress);
                    p.Add("@JumlahDPP", param.vatBaseAmount);
                    p.Add("@JumlahPPN", param.vatAmount);
                    p.Add("@JumlahPPNBM", param.jumlahPPnBm);
                    p.Add("@StatusApprovalXML", param.statusApprovalXml);
                    p.Add("@StatusFakturXML", param.statusFakturXml);
                    p.Add("@FakturType", param.FakturType);
                    p.Add("@DJPXml", urldjp);
                    p.Add("@CreatedOn", DateTime.Now.FormatSqlDateTime());
                    p.Add("@CreatedBy", "TOWASS");
                    p.Add("@Requesttp_VATInDetail", CreateTblVatInDetail(param.vatInDetails).AsTableValuedParameter("dbo.tp_VATInDetail"));
                    p.Add("@datasource", "TOWASS");
                    cnn.Execute("sp_SubmitFakturPajak", p, null, 500, CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }

        public void UpdateDjpXmlFile(Int64 id, string errorMessage)
        {
            try
            {
                IDbConnection cnn = OpenConnection();
                try
                {
                   
                    var p = new DynamicParameters();
                    p.Add("@id", id);
                    p.Add("@ErrorMessage", errorMessage);
                    


                    cnn.Execute("sp_UpdateDjpXmlFile", p, null, 500, CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }


        public Result ValidateVATInInvoiceNumber(string nomorFakturGabungan, 
            string nomorFaktur,
            string fGPengganti,
            string fakturType,
            string nPWPPenjual,
            string invoiceDate)
        {
            try
            {
                IDbConnection cnn = OpenConnectionEFB();
                try
                {
                    var p = new DynamicParameters();

                    p.Add("@NomorFakturGabungan", nomorFakturGabungan);
                    p.Add("@NomorFaktur", nomorFaktur);
                    p.Add("@FGPengganti", fGPengganti);
                    p.Add("@FakturType", fakturType);
                    p.Add("@NPWPPenjual", nPWPPenjual);
                    p.Add("@InvoiceDate", invoiceDate);


                    return cnn.Query<Result>("dbo.usp_ValidateVATInInvoiceNumberTowass", p, null, true, 500, CommandType.StoredProcedure).AsEnumerable().FirstOrDefault();

                   
                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }



        private static DataTable CreateTblVatInDetail(List<VatInDetailManualInputView> param)
        {
            var table = new DataTable();

            table.Columns.Add("NamaBarang", typeof(string));
            table.Columns.Add("HargaSatuan", typeof(decimal));
            table.Columns.Add("JumlahBarang", typeof(decimal));
            table.Columns.Add("HargaTotal", typeof(decimal));
            table.Columns.Add("Diskon", typeof(decimal));
            table.Columns.Add("DPP", typeof(decimal));
            table.Columns.Add("PPN", typeof(decimal));
            table.Columns.Add("TarifPPNBM", typeof(decimal));
            table.Columns.Add("PPNBM", typeof(decimal));
            table.Columns.Add("Status", typeof(string));

            foreach (var val in param)
            {
                DataRow dr = table.NewRow();


                dr["NamaBarang"] = val.NamaBarang;
                dr["HargaSatuan"] = val.HargaSatuan ?? 0;
                dr["JumlahBarang"] = val.JumlahBarang ?? 0;
                dr["HargaTotal"] = val.HargaTotal ?? 0;
                dr["Diskon"] = val.Diskon ?? 0;
                dr["DPP"] = val.dpp ?? 0;
                dr["PPN"] = val.ppn ?? 0;
                dr["TarifPPNBM"] = val.tarifPpnbm ?? 0;
                dr["PPNBM"] = val.ppnbm ?? 0;
                dr["Status"] = val.Status ?? "";
                table.Rows.Add(dr);
            }
            return table;
        }


       

        public void SubmitFakturTms(VatInManualInputView param, string urldjp)
        {
            try
            {
                IDbConnection cnn = OpenConnectionEFB();
                try
                {
                    Guid vatInId = Guid.NewGuid();
                    Guid syncId = Guid.NewGuid();
                    DateTime expireDate = Convert.ToDateTime(param.ExpireDate.Trim());
                    var p = new DynamicParameters();
                    p.Add("@Id", vatInId);
                    p.Add("@SyncId", syncId);
                    p.Add("@IdRfp", param.IdRfp);
                    p.Add("@InvoiceNo", param.InvoiceNoRfp);
                    p.Add("@FileName", param.FileName);
                    p.Add("@KDJenisTransaksi", param.kdJenisTransaksi);
                    p.Add("@FGPengganti", param.fgPengganti);
                    p.Add("@NomorFaktur", param.InvoiceNumber);
                    p.Add("@NomorFakturGabungan", param.InvoiceNumberFull);
                    p.Add("@TanggalFaktur", param.InvoiceDate);
                    p.Add("@ExpireDate", expireDate);
                    p.Add("@NPWPPenjual", param.supplierNpwp);
                    p.Add("@NamaPenjual", param.SupplierName);
                    p.Add("@AlamatPenjual", param.SupplierAddress);
                    p.Add("@JumlahDPP", param.vatBaseAmount);
                    p.Add("@JumlahPPN", param.vatAmount);
                    p.Add("@JumlahPPNBM", param.jumlahPPnBm);
                    p.Add("@StatusApprovalXML", param.statusApprovalXml);
                    p.Add("@StatusFakturXML", param.statusFakturXml);
                    p.Add("@FakturType", param.FakturType);
                    p.Add("@DJPXml", urldjp);
                    p.Add("@CreatedOn", DateTime.Now.FormatSqlDateTime());
                    p.Add("@CreatedBy", "TOWASS");
                    p.Add("@Requesttp_VATInDetail", CreateTblVatInDetail(param.vatInDetails).AsTableValuedParameter("dbo.tp_VATInDetail"));
                    p.Add("@datasource", "TOWASS");
                    cnn.Execute("sp_SubmitFakturPajak", p, null, 500, CommandType.StoredProcedure);
                }
                catch (Exception ex)
                {
                    throw new ArgumentNullException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException(ex.Message);
            }
        }
    }
}
