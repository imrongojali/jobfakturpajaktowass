﻿using Dapper;
using eFakturTowass4.Common.Config;
using System;
using System.Data;
using System.Linq;
using System.Windows.Interop;
using eFakturTowass4.Common.Function;

namespace eFakturTowass4.Repositories.Config
{


    public class ConfigRepository : BaseRepository, IConfigRepository
    {

        //public Models.Config.Config GetByConfigKey(String ConfigKey)
        //{
        //    return executeProcedureQuery<Config>("TAM_EFAKTUR.dbo.usp_GetConfigurationDataByConfigKey", new { ConfigKey = ConfigKey }).FirstOrDefault();
        //}

      

        public Models.Config GetConfigValuesEFB(string configKey)
        {
            try
            {
                IDbConnection cnn = OpenConnectionEFB();
                try
                {
                    var p = new DynamicParameters();

                    p.Add("@ConfigKey", configKey);
                    return cnn.Query<Models.Config>("dbo.usp_GetConfigurationDataByConfigKey", p, null, true, 500, CommandType.StoredProcedure).AsEnumerable().FirstOrDefault();


                }
                catch (Exception ex)
                {
                    throw new CustomException(ex.Message);
                }
                finally
                {
                    cnn?.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new CustomException(ex.Message);
            }
        }


    }
}
