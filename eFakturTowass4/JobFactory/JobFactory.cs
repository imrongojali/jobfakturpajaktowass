﻿using Quartz;
using Quartz.Spi;
using System;

namespace eFakturTowass4.JobFactory
{
    internal class JobFakturFactory : IJobFactory
    {
        private readonly IServiceProvider service;
        public JobFakturFactory(IServiceProvider serviceProvider)
        {
            service = serviceProvider;
        }
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var jobDetail = bundle.JobDetail;
            return (IJob)service.GetService(jobDetail.JobType);
        }

        public void ReturnJob(IJob job)
        {

        }
    }
}
