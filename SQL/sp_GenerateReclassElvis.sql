USE [TOWASS]
GO
/** Object:  StoredProcedure [dbo].[sp_GenerateReclassElvis]    Script Date: 10/25/2023 2:01:11 PM **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_GenerateReclassElvis]
    @ProcessId VARCHAR(20) ,
    @Wcarno VARCHAR(7) ,
    --@paramReclass dbo.tp_ParamReclass READONLY ,
    @user VARCHAR(50) ,
    @error INT OUTPUT
AS
    BEGIN

	

        IF OBJECT_ID('tempdb.dbo.#TempReclass', 'U') IS NOT NULL
            DROP TABLE #TempReclass
        CREATE TABLE #TempReclass
            (
              [Id] [BIGINT] NOT NULL
                            IDENTITY(1, 1)
                            PRIMARY KEY ,
              [ElvisIssuingDivision] [VARCHAR](5) NULL ,
              [BeforeCostCenterCd] [VARCHAR](10) NULL ,
              [BeforeWbsNo] [VARCHAR](25) NULL ,
              [BeforeGlAccount] [INT] NULL ,
              [BeforeAmount] [DECIMAL](18, 0) NULL ,
              [IsBeforeDebit] [BIT] NULL ,
              [ClaimType] [VARCHAR](10) NULL ,
              [ClaimNo] [VARCHAR](15) NULL ,
              [AfterCostCenterCd] [VARCHAR](10) NULL ,
              [AfterWbsNo] [VARCHAR](25) NULL ,
              [AfterGlAccount] [INT] NULL ,
              [AfterAmount] [DECIMAL](18, 0) NULL ,
              [IsAfterDebit] [BIT] NULL ,
              [FiscalYear] [INT] NULL ,
              [CreatedDate] [DATETIME] NULL ,
              [CreatedBy] [VARCHAR](50) NULL
            )
        INSERT  INTO #TempReclass
                ( ElvisIssuingDivision ,
                  BeforeCostCenterCd ,
                  BeforeWbsNo ,
                  BeforeGlAccount ,
                  BeforeAmount ,
                  IsBeforeDebit ,
                  ClaimType ,
                  ClaimNo ,
                  AfterCostCenterCd ,
                  AfterWbsNo ,
                  AfterGlAccount ,
                  AfterAmount ,
                  IsAfterDebit ,
                  FiscalYear ,
                  CreatedDate ,
                  CreatedBy
	            )

			
                SELECT DISTINCT
                        mep.ElvisIssuingDivision ,
                        mct.CostCenter BeforeCostCenterCd ,
                        mep.WbsChargeIn BeforeWbsNo ,
                        CONVERT(INT, mct.AccruedReceivableAccount) BeforeGlAccount ,
                       -- rst.ClaimTotGrandIdr BeforeAmount ,
                        ISNULL(CASE WHEN ISNULL(msc.SystemValue, '') = ''
                                    THEN
												(coalesce(rst.CLAIMTOTAMOUNT, 0.00) + CONVERT(NUMERIC(19,2),coalesce((rst.CLAIMTOTAMOUNT *
												(
													select top 1 systemvalue from mastersystemconfig where SystemCategory='TAM' and SystemSubCategory='TAX' and SystemCode='idr' and isdeleted=0
												)), 0.00)))
                                    ELSE rst.ClaimTotGrandUsd
                               END, 0) BeforeAmount ,
                        CONVERT(BIT, 0) IsBeforeDebit ,
                        rst.ClaimType ClaimType ,
                        pr.WcarNo ClaimNo ,
                        mep.CostCenter AfterCostCenterCd ,
                        mep.WbsChargeOut AfterWbsNo ,
                        CONVERT(INT, mep.GlNo) AfterGlAccount ,
                        --rst.ClaimTotGrandIdr AfterAmount ,
                        ISNULL(CASE WHEN ISNULL(msc.SystemValue, '') = ''
                                    THEN (coalesce(rst.CLAIMTOTAMOUNT, 0.00) + CONVERT(NUMERIC(19,2),coalesce((rst.CLAIMTOTAMOUNT *
												(
													select top 1 systemvalue from mastersystemconfig where SystemCategory='TAM' and SystemSubCategory='TAX' and SystemCode='idr' and isdeleted=0
												)), 0.00)))
                                    ELSE rst.ClaimTotGrandUsd
                               END, 0) AfterAmount ,
                        CONVERT(BIT, 1) IsAfterDebit ,
                        YEAR(GETDATE()) FiscalYear ,
                        GETDATE() CreatedDate ,
                        @user CreatedBy
                FROM    dbo.RSw103Tam AS rst
                        INNER JOIN dbo.MasterExternalParties AS mep ON rst.EpCodeVendor = mep.EpCode
                        INNER JOIN dbo.RWcarTwc AS pr ON rst.TwcNo = pr.TwcNo
                        INNER JOIN dbo.RWcar AS rw ON pr.WcarNo = rw.WcarNo
                        INNER JOIN dbo.MasterClaimTypes AS mct ON mct.ClaimType = rst.ClaimType
                        LEFT JOIN dbo.MasterSystemConfig AS msc ON msc.SystemCategory = 'PARAMETER'
                                                              AND msc.SystemSubCategory = 'VENDOR_NON_LOCAL_AC'
                                                              AND rw.EpCodeVendor = msc.SystemValue
                                                              AND msc.Active = 1
                WHERE   rw.WcarNo = @Wcarno
                        AND rst.IsDeleted = 0
                        AND mep.IsDeleted = 0
                        AND mct.IsDeleted = 0
                        AND rw.IsDeleted = 0
                        AND ISNULL(msc.IsDeleted, 0) = 0

						

 

        DECLARE @_err INT= 0

        IF EXISTS ( SELECT  *
                    FROM    #TempReclass )
            BEGIN

                IF OBJECT_ID('tempdb.dbo.#TempReclassProcess', 'U') IS NOT NULL
                    DROP TABLE #TempReclassProcess
                CREATE TABLE #TempReclassProcess
                    (
                      ProcessId VARCHAR(20) NULL
                    )
                INSERT  INTO #TempReclassProcess
                        ( ProcessId
                        )
                        SELECT  a.ProcessId
                        FROM    TOWASS_SYNC.dbo.TB_SYNC_RECLASS a
                                INNER JOIN TOWASS_SYNC.dbo.TB_SYNC_RECLASS_DETAIL b ON a.ProcessId = b.ProcessId
                        WHERE   b.ClaimNo = @Wcarno
                                AND a.FlagStagging = 0

                IF EXISTS ( SELECT  *
                            FROM    #TempReclassProcess )
                    BEGIN
                        
						
		

                        DELETE  FROM TOWASS_SYNC.dbo.TB_SYNC_RECLASS
                        WHERE   ProcessId IN ( SELECT   ProcessId
                                               FROM     #TempReclassProcess )

                        DELETE  FROM TOWASS_SYNC.dbo.TB_SYNC_RECLASS_ATTACHMENT
                        WHERE   ProcessId IN ( SELECT   ProcessId
                                               FROM     #TempReclassProcess )

                        DELETE  FROM TOWASS_SYNC.dbo.TB_SYNC_RECLASS_DETAIL
                        WHERE   ProcessId IN ( SELECT   ProcessId
                                               FROM     #TempReclassProcess )
                    END

                INSERT  INTO TOWASS_SYNC.dbo.TB_SYNC_RECLASS_DETAIL
                        ( ProcessId ,
                          BeforeCostCenterCd ,
                          BeforeWbsNo ,
                          BeforeGlAccount ,
                          BeforeAmount ,
                          IsBeforeDebit ,
                          ClaimType ,
                          ClaimNo ,
                          AfterCostCenterCd ,
                          AfterWbsNo ,
                          AfterGlAccount ,
                          AfterAmount ,
                          IsAfterDebit ,
                          FiscalYear ,
                          CreatedDate ,
                          CreatedBy
						
                        )
                        SELECT DISTINCT
                                @ProcessId ,
                                tr.BeforeCostCenterCd ,
                                tr.BeforeWbsNo ,
                                tr.BeforeGlAccount ,
                                SUM(tr.BeforeAmount) BeforeAmount ,
                                tr.IsBeforeDebit ,
                                tr.ClaimType ,
                                tr.ClaimNo ,
                                tr.AfterCostCenterCd ,
                                tr.AfterWbsNo ,
                                tr.AfterGlAccount ,
                                SUM(tr.AfterAmount) AfterAmount ,
                                tr.IsAfterDebit ,
                                tr.FiscalYear ,
                                GETDATE() ,
                                @user
                        FROM    #TempReclass AS tr
                        GROUP BY tr.BeforeCostCenterCd ,
                                tr.BeforeWbsNo ,
                                tr.BeforeGlAccount ,
                                tr.IsBeforeDebit ,
                                tr.ClaimType ,
                                tr.ClaimNo ,
                                tr.AfterCostCenterCd ,
                                tr.AfterWbsNo ,
                                tr.AfterGlAccount ,
                                tr.IsAfterDebit ,
                                tr.FiscalYear 

						


			
                INSERT  INTO TOWASS_SYNC.dbo.TB_SYNC_RECLASS
                        ( ProcessId ,
                          IssuingDivision ,
                          ReclassStatus ,
                          PICCurrent ,
                          TotalAmount ,
                          DocSAPNo ,
                          FlagStagging ,
                          CreatedDate ,
                          CreatedBy ,
                          SubmitToElvisDate
						
                        )
                        SELECT DISTINCT
                                @ProcessId ,
                                tr.ElvisIssuingDivision ,
                                '0' ,
                                '' ,
                                SUM(tr.BeforeAmount) BeforeAmount ,
                                '' ,
                                0 ,
                                GETDATE() ,
                                @user ,
                                GETDATE()
                        FROM    #TempReclass AS tr
                        GROUP BY tr.ElvisIssuingDivision 
						
                        
              

                INSERT  INTO TOWASS_SYNC.dbo.TB_SYNC_RECLASS_ATTACHMENT
                        ( ProcessId ,
                          ClaimType ,
                          ClaimNo ,
                          SeqNo ,
                          FileName
				        )
                        SELECT  b.ProcessId ,
                                b.ClaimType ,
                                b.ClaimNo ,
                                ROW_NUMBER() OVER ( ORDER BY b.ClaimNo DESC ) AS SeqNo ,
                                b.WcarAttachment
                        FROM    ( SELECT DISTINCT
                                            @ProcessId ProcessId ,
                                            pr.ClaimType ,
                                            pr.ClaimNo ClaimNo ,
                                            a.WcarAttachment
                                  FROM      RWcarTwc AS a
                                            INNER JOIN #TempReclass AS pr ON a.WcarNo = pr.ClaimNo
                                  UNION
                                  SELECT DISTINCT
                                            @ProcessId ProcessId ,
                                            pr.ClaimType ,
                                            pr.ClaimNo ClaimNo ,
                                            a.WcarAttachmentDetail
                                  FROM      RWcarTwc AS a
                                            INNER JOIN #TempReclass AS pr ON a.WcarNo = pr.ClaimNo
                                ) b
                        

                SET @_err = 0

				--SELECT mct.CostCenter,mct.AccruedReceivableAccount,*FROM dbo.MasterClaimTypes AS mct WHERE mct.ClaimType='TWC'

				--SELECT*FROM  
            END
        ELSE
            BEGIN
                SET @_err = 1
            END
                        
        SELECT  @error = @_err
                        

						

    END