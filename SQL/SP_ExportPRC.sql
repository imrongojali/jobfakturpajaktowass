USE [TOWASS]
GO
/** Object:  StoredProcedure [dbo].[SP_ExportPRC]    Script Date: 10/23/2023 2:34:36 PM **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_ExportPRC]
    @ClaimNo Varchar (50),
    @twcno Varchar (50),
    @WcarDealerNo Varchar (50),
    @EpCodeDealer Varchar (50),
	@PartNo Varchar (50),
	@fromdate Varchar (50),
	@todate Varchar (50),
	@claimType Varchar (50),
	@endStatus Varchar (50),
	@prcStatus Varchar (50),
	@receiveStatus Varchar(50),
	@bookingTicket Varchar(50),
	@filteringSts Varchar(50),
	@jugementresult Varchar(50),
	@userid varchar(30) = null
AS
BEGIN
	SET NOCOUNT ON
    DECLARE @command VARCHAR(MAX);
    DECLARE @commandCount VARCHAR(MAX);

	DECLARE @countclaimlist INT = 0;
		SELECT  @countclaimlist = count(y.ClaimType)
		FROM    MasterUserClaimType x
		INNER JOIN dbo.MasterClaimTypes y ON x.ClaimId = y.Id
		WHERE   x.UserId = @userid AND x.IsDeleted =0 AND x.Active = 1 AND y.Active = 1 AND y.IsDeleted =0

    SET @receiveStatus = CASE WHEN @receiveStatus = '0' THEN 'N'
                               WHEN @receiveStatus = '1' THEN 'Y'
                               ELSE @receiveStatus
                          END
    SET @filteringSts = CASE WHEN @filteringSts = '0' THEN 'N'
                             WHEN @filteringSts = '1' THEN 'Y'
                             ELSE @filteringSts
                        END
    SET @command = N'
 SELECT P.Id ,
        D.WcarDealerNo ,
        S.EpCodeDealer ,
        S.ClaimNo ,
        S.TwcNo ,
        LTRIM(RTRIM(S.WMI)) + LTRIM(RTRIM(S.VDS)) + LTRIM(RTRIM(S.CD))
        + LTRIM(RTRIM(S.VIS)) AS VIN ,
        e.ModelCode AS modelcode ,
        S.RepairDate AS repairdate ,
        P.PartNo ,
        P.PartUniqCode AS PartCode ,
        P.SeqNo ,
        P.Qty ,
        P.PrcStatus ,
        M2.SystemValue PrcStatusDescription ,
        P.BookingNumber ,
		P.MoveOutDate,
        P.EndResult ,
        S.Caused ,
        f.Name AS partname ,
        S.ClaimType ,
        P.ReceivingQty ,
        P.FilterringQty ,
        M.SystemValue AS endstatus ,
        M.SystemCode AS END_STATUS_CD ,
        P.prrstatus AS StatusPRR ,
        P.ModifiedAt ,
        CASE WHEN P.FilterringQty > 0 THEN ''Y''
             ELSE ''N''
        END AS filterstatus ,
        CASE P.ReceiveStatus
          WHEN ''1'' THEN ''Y''
          WHEN ''0'' THEN ''N''
          ELSE ''''
        END STATUS ,
		p.ReceiveDate
 FROM      RSw103Parts P  WITH ( NOLOCK )
                        INNER JOIN Rsw103 S  WITH ( NOLOCK ) ON P.ClaimNo = S.ClaimNo
                                               AND P.EpCodeDealer = S.EpCodeDealer
                                               AND S.IsDeleted = 0
                        inner JOIN RWcarSw103Dealer D  WITH ( NOLOCK ) ON S.EpCodeDealer = D.EpCodeDealer
                                                        AND S.ClaimNo = D.ClaimNo
                        LEFT JOIN MasterSystemConfig M  WITH ( NOLOCK ) ON M.SystemCategory = ''PARAMETER''
                                                          AND M.SystemSubCategory = ''END_STATUS''
                                                          AND M.SystemCode = P.EndStatus
                        LEFT JOIN MasterSystemConfig M2  WITH ( NOLOCK ) ON M2.SystemCategory = ''COMMON''
                                                           AND M2.SystemSubCategory = ''STATUS_PRC''
                                                           AND CONVERT(INT, ISNULL(M2.SystemCode,
                                                              ''0'')) = P.PrcStatus
                        left JOIN MasterVehicles e  WITH ( NOLOCK ) ON e.Vds = S.VDS
                                                      AND e.Wmi = S.WMI
                                                      AND e.Cd = S.CD
                                                      AND e.Vis = S.VIS
                        left JOIN MasterPart f  WITH ( NOLOCK ) ON f.PartNo = P.PartNo
						
						  WHERE     1 = 1
            AND p.IsDeleted = 0
            AND ISNULL(s.TwcNo,'''')!='''' 
				'
IF ISNULL(@fromdate, '') != ''
        SET @command = @command
            + N' AND convert(date, p.ReceiveDate,103) >=  ''' + @fromdate
            + ''''
    IF ISNULL(@todate, '') != ''
        SET @command = @command
            + N' AND convert(date, p.ReceiveDate,103) <=  ''' + @todate + ''''
    IF ISNULL(@EpCodeDealer, '') != ''
        SET @command = @command + N' AND S.EpCodeDealer  =  '''
            + @EpCodeDealer + ''''
    IF ISNULL(@twcno, '') != ''
        SET @command = @command + N' AND S.TwcNo = ''' + @twcno + ''''
    IF ISNULL(@WcarDealerNo, '') != ''
        SET @command = @command + N' AND  D.WcarDealerNo = '''
            + @WcarDealerNo + ''''
    IF ISNULL(@ClaimNo, '') != ''
        SET @command = @command + N' AND  S.ClaimNo = ''' + @ClaimNo + ''''
    IF ISNULL(@PartNo, '') != ''
        SET @command = @command + N' AND P.PartNo = ''' + @PartNo + ''''
    IF ISNULL(@claimType, '') != ''
        SET @command = @command + N' AND S.ClaimType = ''' + @claimType
            + ''''
	IF ISNULL(@endStatus, '') != ''
        SET @command = @command + N' AND M.SystemCode = ''' + @endStatus
            + ''''
    IF ISNULL(@prcStatus, '') != ''
        SET @command = @command + N' AND P.PrcStatus  = ''' + @prcStatus
            + ''''

    IF ISNULL(@receiveStatus, '') != ''
        SET @command = @command + N' AND  CASE P.ReceiveStatus
          WHEN ''1'' THEN ''Y''
          WHEN ''0'' THEN ''N''
          ELSE ''''
        END  = ''' + @receiveStatus
            + ''''

    IF ISNULL(@bookingTicket, '') != ''
        SET @command = @command + N' AND P.BookingNumber  = '''
            + @bookingTicket + ''''

    IF ISNULL(@filteringSts, '') != ''
        SET @command = @command + N' AND CASE WHEN P.FilterringQty > 0 THEN ''Y''
             ELSE ''N''
        END = '''
            + @filteringSts + ''''

    IF ISNULL(@jugementresult, '') != ''
        SET @command = @command + N' AND P.EndResult = ''' + @jugementresult
            + ''''

	IF(@countclaimlist > 0)
		      SET @command = @command + N' AND S.ClaimType IN (SELECT  y.ClaimType
											FROM    MasterUserClaimType x
											INNER JOIN dbo.MasterClaimTypes y ON x.ClaimId = y.Id
											WHERE   x.UserId = ''' + @userid + ''' AND x.IsDeleted =0 AND x.Active = 1 AND y.Active = 1 AND y.IsDeleted =0
											) '

    SET @command = @command + N' ORDER BY p.id DESC'

    PRINT ( @command )
    EXEC(@command)
END