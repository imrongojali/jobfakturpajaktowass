USE [TempTowass]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDjpXmlFile]    Script Date: 10/23/2023 10:06:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
																					
ALTER PROCEDURE [dbo].[sp_CreateDjpXmlFile] 
@idVatIn bigint,
@InvoiceNo varchar(50),
@FileNameXML nvarchar(300),
@UrlDJPQR nvarchar(MAX),
@FileNameDJP nvarchar(300)

AS
    BEGIN																							
        SET NOCOUNT ON;	
		IF EXISTS
    (
    SELECT 1
    FROM [dbo].[DjpXmlFile]
     where [InvoiceNo]=@InvoiceNo
    AND [JobFlag] = 0
    )
	begin
	update [dbo].[DjpXmlFile] set [FileNameXML]=@FileNameXML, [UrlDJPQR]=@UrlDJPQR, [FileNameDJP]=@FileNameDJP 
	where [InvoiceNo]=@InvoiceNo
    AND [JobFlag] = 0
	end
	else
	begin
		declare @SeqNo int
		select @SeqNo=isnull(max([SeqNo]),0)+1 from [dbo].[DjpXmlFile] where [InvoiceNo]=@InvoiceNo
		insert into [dbo].[DjpXmlFile] (IdVatIn,
	   [SeqNo]
      ,[InvoiceNo]
      ,[FileNameXML]
      ,[UrlDJPQR]
      ,[FileNameDJP]
      ,[JobFlag]
      ,[CreatedAt])
	  values(
	  @idVatIn,
	  @SeqNo,
	  @InvoiceNo,
	  @FileNameXML,
	  @UrlDJPQR,
	  @FileNameDJP,
	  0,
	  getdate()
	  )
	  end
		end