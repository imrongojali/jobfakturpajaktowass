USE [TempTowass]
GO

/****** Object:  Table [dbo].[DjpXmlFile]    Script Date: 10/23/2023 10:07:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DjpXmlFile](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdVatIn] [bigint] NULL,
	[SeqNo] [int] NOT NULL,
	[InvoiceNo] [varchar](50) NOT NULL,
	[FileNameXML] [nvarchar](300) NOT NULL,
	[UrlDJPQR] [nvarchar](max) NOT NULL,
	[FileNameDJP] [nvarchar](300) NOT NULL,
	[JobFlag] [bit] NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NOT NULL,
 CONSTRAINT [PK_DjpXmlFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


