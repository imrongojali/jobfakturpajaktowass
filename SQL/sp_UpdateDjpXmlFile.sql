USE [TempTowass]
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDjpXmlFile]    Script Date: 10/23/2023 10:02:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
																					
create PROCEDURE [dbo].[sp_UpdateDjpXmlFile] @id bigint, @ErrorMessage nvarchar(MAX)


AS
    BEGIN																							
        SET NOCOUNT ON;	
		
	update [dbo].[DjpXmlFile] set [JobFlag] = 1, ErrorMessage=@ErrorMessage
	where [Id]=@id
   
		end