USE [TAM_EFAKTUR]
GO
/** Object:  StoredProcedure [dbo].[usp_ValidateVATInInvoiceNumber]    Script Date: 15/03/2023 01:01:38 **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[usp_ValidateVATInInvoiceNumberTowass]
	@NomorFakturGabungan as VARCHAR(100),
	@NomorFaktur as varchar(20),
	@FGPengganti as varchar(2),
	@FakturType as varchar(20),
	@NPWPPenjual as varchar(50),
	@InvoiceDate as varchar(30)
AS
BEGIN
	DECLARE 
	@ResultCode bit = 0,
	@ResultDesc VARCHAR(260) = '',
	@CountFaktur as varchar(150),
	@CountFakturDelete as varchar(400),
	@GetNPWP as varchar(400)
	, @ResultPVNumber VARCHAR(260) = ''

	Set @CountFaktur = (select COUNT(NomorFaktur) from TB_R_VATIn where FGPengganti = '0' and NomorFaktur = @NomorFaktur and RowStatus = 0)
	Set @CountFakturDelete = (select COUNT(NomorFaktur) from TB_R_VATIn where FGPengganti = '0' and NomorFakturGabungan = @NomorFakturGabungan and RowStatus = 1)
	set @GetNPWP = (SELECT TOP 1 NPWPPenjual from TB_R_VATIn where NomorFaktur = @NomorFaktur and FGPengganti = '0')

	-- Convert date 20190807
	declare @isDate int
	set @isDate = (select isdate(@InvoiceDate))
	if(@isDate=0)
	begin
		declare @InvoiceDatePar as varchar(30)
		set @InvoiceDatePar = @InvoiceDate
		set @InvoiceDate = convert(datetime, @InvoiceDatePar, 103)
	end
	-- End 	

	IF EXISTS 
	(
		SELECT 1 FROM TB_R_VATIn WHERE 
			NomorFakturGabungan = @NomorFakturGabungan 
		AND
			@FGPengganti = '0'
		AND 
			(ApprovalStatus IS NULL OR ApprovalStatus <> 'Reject')
		AND 
			RowStatus = 0
	)
	BEGIN
		SET @ResultCode = 0

		SELECT top 1 @ResultPVNumber = PVNumber FROM TB_R_VATIn WHERE 
			NomorFakturGabungan = @NomorFakturGabungan 
		AND	@FGPengganti = '0'
		AND (ApprovalStatus IS NULL OR ApprovalStatus <> 'Reject')
		AND RowStatus = 0

		if (isnull(@ResultPVNumber,'')!='')
		begin
			SET @ResultDesc = 'Tax Invoice Number: '+@NomorFakturGabungan+' already use in PV Number: '+@ResultPVNumber
		end
		
	END
	

	ELSE IF @CountFaktur = 0 and @FGPengganti != '0' and @FakturType = 'eFaktur'
	BEGIN
		SET @ResultCode = 0
		SET @ResultDesc = 'Please scan faktur pajak normal'
	END
	
	ELSE IF @CountFakturDelete = 1
	BEGIN

		delete from tb_r_vatindetail where 
		vatinid in (select id from tb_r_vatin where nomorfakturgabungan = @nomorfakturgabungan)

		delete from tb_r_vatin where 
		nomorfakturgabungan = @nomorfakturgabungan

		SET @ResultCode = 1
		SET @ResultDesc = 'Success'

	END

	ELSE IF @GetNPWP != @NPWPPenjual and @FakturType = 'eFaktur'
	BEGIN
		
		BEGIN
		SET @ResultCode = 0
		SET @ResultDesc = 'Supplier NPWP Doesnt Match with Faktur Pajak Normal'
	END
	END

	ELSE if @FGPengganti = '1' and @FakturType = 'eFaktur' and EXISTS
		(
			SELECT 1 FROM (select top 1 TanggalFaktur from TB_R_VATIn 
							WHERE NomorFaktur = @NomorFaktur 
							AND (ApprovalStatus IS NULL OR ApprovalStatus <> 'Reject')
							AND RowStatus = 0 
							order by TanggalFaktur desc) a 
			WHERE a.TanggalFaktur > cast(@InvoiceDate as date)
		)
	BEGIN
		SET @ResultCode = 0
		SET @ResultDesc = 'Tax Invoice Date must greater or equal to existing date input before'
	END

	ELSE
	BEGIN
		SET @ResultCode = 1
		SET @ResultDesc = 'Success'
	END

	SELECT @ResultCode AS ResultCode, @ResultDesc AS ResultDesc

END